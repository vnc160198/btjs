/**
 * <input>
 *
 *
 * người dùng nhập vào giá trị lần lượt của 5 số
 *
 *
 *
 * <các bước xử lý>
 *
 * b1 : tạo lần lượt 5 biến number1, number2, number3, number4, number5;
 * và biến gtTB .
 * b2 : gán giá trị cho 5 biến trên : number1, number2, number3, number4, number5
 * b3 : áp dụng công thức :
 *   gtTB = (number1 + number2 + number3 + number4 + number5) /5
 *
 * b4: in giá trị ra console.
 * <ouput>
 *  kết quả giá trị trung bình của 5 số đã nhập .
 */
var number1 = 16;
var number2 = 1;
var number3 = 8;
var number4 = 6;
var number5 = 7;
gtTB = (number1 + number2 + number3 + number4 + number5) / 5;
console.log(
  "5 số đã nhập vào là :",
  number1,
  number2,
  number3,
  number4,
  number5
);
console.log("=> Giá trị trung bình là :", gtTB);
