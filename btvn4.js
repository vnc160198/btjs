/**
 * <input>
 *
 * người dùng nhập vào :
    Chiều dài và chiều rộng của hình chữ nhật.
 *
 *
 * <các bước xử lý>
 * b1: Tạo các biến : 
 *    chieuDai , chieuRong ,chuvi và dienTich.
 * b2: gán giá trị cho 2 biến chieuDai và chieuRong.
 * b3 : áp dụng công thức :
 *   chuvi = (chieuDai + chieuRong) * 2;
 *    dienTich = chieuDai * chieuRong;
 * b4: in kết quả ra console gồm chu vi và diện tích .
 * <ouput>
 *  kết quả chu vi và diện tích của HCN
 *
 */
var chieuDai = 20;
var chieuRong = 10;
var chuvi, dienTich;

chuvi = (chieuDai + chieuRong) * 2;
dienTich = chieuDai * chieuRong;
console.log("Thông sô của hình chữ nhật là :");
console.log("Chiều dài :", chieuDai, "Chiều rộng", chieuRong);
console.log("=> Chu vi HCN : ", chuvi, "Diện tích HCN :", dienTich);
