/**
 *
 * <input>
 *
 * người dùng nhập vào :
 *  số thứ nhất và số thứ 2 cần tính
 *
 *
 * <các bước xử lý>
 * b1 : tạo các biến :
 *    number1, number2, donVi, chuc,donVi2, chuc2, sum1, sum2,sum
 * b2: gán giá trị cho các biến number1 và number2.
 * b3: áp dụng công thức:
 *   donVi = number1 % 10
 *   chuc = Math.floor(number1 / 10).
 * để tìm chữ số hàng chục và hàng đơn vị của number1
 * áp dụng công thức trên với number2.
 *   tổng 2 kí số của number1:
 *   sum1 = donVi + chuc
 * áp dụng cách trên tính đc sum2
 * ==> tổng các kí số :
 *   sum = sum1 + sum2
 * b4 : in kết quả ra console
 *
 * <ouput>
 *  tổng giá trị các kí số của 2 số đã nhập .
 *
 */
var number1 = 59;
var number2 = 23;
// var number2 = 13;
var donVi, chuc, donVi2, chuc2;
sum1 = 0;
sum2 = 0;
sum = 0;
donVi = number1 % 10;
chuc = Math.floor(number1 / 10);
sum1 = donVi + chuc;

donVi2 = number2 % 10;
chuc2 = Math.floor(number2 / 10);
sum2 = donVi2 + chuc2;
sum = sum1 + sum2;
console.log("2 số vừa nhập là :", number1, number2);
console.log("=> Tổng kí số 2 số vừa nhập là :", sum);
