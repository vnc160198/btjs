/**
 * <input>
 *
 * người dùng nhập vào :
 * số ngày làm
 * tiền lương 1 ngày
 *
 * <các bước xử lý>
 *
 * b1: tạo 3 biến : soNgayLam , luong1ngay và luong
 *
 * b2:gán giá trị cho 2 biến soNgayLam và luong1ngay
 * b3 : sử dụng công thức : luong = soNgayLam * luong1ngay;
 * b4: ỉn kết quả luong ra console.
 * <ouput>
 *
 * kết quả lương nhận được :luong
 *
 *
 *
 *
 */
var soNgayLam = 5;
var luong1ngay = 100000;
luong = luong1ngay * soNgayLam;

console.log("Số ngày làm :", soNgayLam);
console.log("Lương 1 ngày :", luong1ngay);
console.log(" => Lương nhận :", luong);
