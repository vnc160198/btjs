/**
 * <input>
 *
 *
 * người dùng nhập vào :
 * số tiền USD cần qui đổi
 *
 *
 *
 * <các bước xử lý>
 * b1 : tạo biến usd , soTien và tienQuyDoi.
 * b2 : gán giá trị cho biến usd và soTien.
 * b3: áp dụng công thức :
 *    tienQuyDoi = usd * soTien
 * b4: in kết quả ra console.
 * <ouput>
 *
 * số tiền quy đổi được
 */
var usd = 23500;
var soTien = 2;
var tienQuyDoi = usd * soTien;
console.log("Số tiền cần đổi :", soTien, "USD");
console.log("=> Số tiền đổi được :", tienQuyDoi, "VNĐ");
